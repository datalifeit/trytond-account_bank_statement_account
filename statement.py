# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal

from sql import Null
from sql.aggregate import Sum
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import Check, Index, ModelSQL, ModelView, fields
from trytond.modules.currency.fields import Monetary
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Equal, Eval, If, Not
from trytond.tools import reduce_ids
from trytond.transaction import Transaction

POSTED_STATES = {
    'readonly': Not(Equal(Eval('state'), 'confirmed'))
    }
_ZERO = Decimal("0.0")


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.bank.statement.line'

    lines = fields.One2Many('account.bank.statement.move.line',
        'line', 'Transactions', states=POSTED_STATES)

    @classmethod
    @ModelView.button
    def post(cls, statement_lines):
        for st_line in statement_lines:
            if not st_line.journal.joined_move:
                for line in st_line.lines:
                    line.create_move()
        super(StatementLine, cls).post(statement_lines)

    @fields.depends('bank_lines', 'state', 'company_currency', 'lines')
    def on_change_with_moves_amount(self, name=None):
        amount = super(StatementLine, self).on_change_with_moves_amount(name)
        if self.state == 'posted':
            return amount
        amount += sum(l.amount or Decimal('0.0') for l in self.lines)
        if self.company_currency:
            amount = self.company_currency.round(amount)
        return amount

    @classmethod
    def _get_moves_amount(cls, lines):
        pool = Pool()
        StatementMoveLine = pool.get('account.bank.statement.move.line')
        stat_move_line = StatementMoveLine.__table__()
        cursor = Transaction().connection.cursor()

        res = super()._get_moves_amount(lines)
        sublines = [l for l in lines if l.state != 'posted']
        cursor.execute(*stat_move_line.select(
                stat_move_line.line,
                Sum(stat_move_line.amount),
                where=(reduce_ids(stat_move_line.line,
                    list(map(int, sublines)))),
                group_by=stat_move_line.line)
        )
        for line_id, amount in cursor.fetchall():
            res[line_id] += Decimal(str(amount))

        return res

    @classmethod
    @ModelView.button
    def cancel(cls, statement_lines):
        super(StatementLine, cls).cancel(statement_lines)
        with Transaction().set_context({
                    'from_account_bank_statement_line': True,
                    }):
            for st_line in statement_lines:
                st_line.reset_account_move()

    def reset_account_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        Reconciliation = pool.get('account.move.reconciliation')
        BankReconciliation = pool.get('account.bank.reconciliation')

        delete_moves = [x.move for x in self.lines if x.move]
        reconciliations = [x.reconciliation for m in delete_moves
            for x in m.lines if x.reconciliation]
        bank_reconciliations = [bl for m in delete_moves
            for x in m.lines for bl in x.bank_lines]
        if reconciliations:
            Reconciliation.delete(reconciliations)
        if bank_reconciliations:
            BankReconciliation.delete(bank_reconciliations)
        if delete_moves:
            Move.draft(delete_moves)
            Move.delete(delete_moves)

    def get_joined_move_lines(self):
        lines, new_lines = super().get_joined_move_lines()
        for line in self.lines:
            lines.append(line)
            new_lines.append(line._get_move_line())
        return lines, new_lines

    def reconcile_joined_move_lines(self, line, new_line):
        super().reconcile_joined_move_lines(line, new_line)
        if (line.__name__ == 'account.bank.statement.move.line'
                and line.invoice):
            line._reconcile_line(new_line)

    def get_joined_bank_move_line(self, lines):
        journal = self.journal
        account = journal.account
        for line in self.lines:
            if line.account == account:
                raise UserError(gettext('account_bank_statement.same_account',
                    account=line.account.rec_name,
                    line=line.rec_name,
                    journal=self.journal.rec_name))
        return super().get_joined_bank_move_line(lines)


class StatementMoveLine(ModelSQL, ModelView):
    'Statement Move Line'
    __name__ = 'account.bank.statement.move.line'

    line = fields.Many2One('account.bank.statement.line', 'Line',
        required=True, ondelete='CASCADE')
    date = fields.Date('Date', required=True)
    currency = fields.Function(
        fields.Many2One('currency.currency', 'Currency'),
        'on_change_with_currency')
    amount = Monetary('Amount', required=True,
        currency='currency', digits='currency')
    party = fields.Many2One('party.party', 'Party',
        states={
            'required': Eval('party_required', False),
            'invisible': ~Eval('party_required', False),
            })
    party_required = fields.Function(fields.Boolean('Party Required'),
        'on_change_with_party_required')
    account = fields.Many2One('account.account', 'Account', required=True,
        domain=[
            ('company', '=', Eval('_parent_line', {}).get('company', 0)),
            ('type', '!=', Null),
            ])
    description = fields.Char('Description')
    move = fields.Many2One('account.move', 'Account Move', readonly=True)
    invoice = fields.Many2One('account.invoice', 'Invoice',
        domain=[
            ('company', '=', Eval('_parent_line', {}).get('company', 0)),
            If(Bool(Eval('party')), [('party', '=', Eval('party'))], []),
            If(Bool(Eval('account')), [('account', '=', Eval('account'))], []),
            If(Eval('_parent_line', {}).get('state') != 'posted',
                ('state', '=', 'posted'),
                ('state', '!=', '')),
            ])

    @classmethod
    def __setup__(cls):
        super(StatementMoveLine, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [(
                'check_bank_move_amount', Check(t, t.amount != 0),
                'Amount should be a positive or negative value.'),
        ]

        cls._sql_indexes.add(
            Index(t, (t.line, Index.Equality())))

    @fields.depends('line', '_parent_line.statement')
    def on_change_with_currency(self, name=None):
        if (self.line
                and self.line.statement
                and self.line.statement.company
                and self.line.statement.company.currency):
            return self.line.statement.company.currency.id

    @fields.depends('_parent_line.date', 'line')
    def on_change_with_date(self):
        if self.line and self.line.date:
            return self.line.date.date()

    @fields.depends('line', '_parent_line.company_amount',
        '_parent_line.moves_amount')
    def on_change_with_amount(self):
        if self.line:
            return self.line.company_amount - (self.line.moves_amount or _ZERO)

    @fields.depends('account')
    def on_change_with_party_required(self, name=None):
        if self.account:
            return self.account.party_required
        return False

    @fields.depends('account', 'amount', 'party', 'invoice')
    def on_change_party(self):
        if self.party and self.amount:
            if self.amount > Decimal("0.0"):
                account = self.account or self.party.account_receivable
            else:
                account = self.account or self.party.account_payable
            self.account = account
        if self.invoice:
            if self.party:
                if self.invoice.party != self.party:
                    self.invoice = None
            else:
                self.invoice = None

    @fields.depends('amount', 'party', 'account', 'invoice',
        '_parent_line.journal', 'line')
    def on_change_amount(self):
        Currency = Pool().get('currency.currency')
        if self.party and not self.account and self.amount:
            if self.amount > Decimal("0.0"):
                account = self.party.account_receivable
            else:
                account = self.party.account_payable
            self.account = account
        if self.invoice:
            if self.amount and self.line and self.line.journal:
                invoice = self.invoice
                journal = self.line.journal
                with Transaction().set_context(date=invoice.currency_date):
                    amount_to_pay = Currency.compute(invoice.currency,
                        invoice.amount_to_pay, journal.currency)
                if abs(self.amount) > amount_to_pay:
                    self.invoice = None
            else:
                self.invoice = None

    @fields.depends('account', 'invoice')
    def on_change_account(self):
        if self.invoice:
            if self.account:
                if self.invoice.account != self.account:
                    self.invoice = None
            else:
                self.invoice = None

    @fields.depends('party', 'account', 'invoice')
    def on_change_invoice(self):
        if self.invoice:
            if not self.party:
                self.party = self.invoice.party
            if not self.account:
                self.account = self.invoice.account

    def get_rec_name(self, name):
        return self.line.rec_name

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default.setdefault('move', None)
        default.setdefault('invoice', None)
        return super(StatementMoveLine, cls).copy(lines, default=default)

    def create_move(self):
        '''
        Create move for the statement line and return move if created.
        '''
        pool = Pool()
        Move = pool.get('account.move')

        if self.move:
            return

        move = self._get_move()
        move.save()
        Move.post([move])

        journal = self.line.journal
        account = journal.account

        st_move_line, = [x for x in move.lines if x.account == account]
        bank_line, = st_move_line.bank_lines
        bank_line.bank_statement_line = self.line
        bank_line.save()

        self.move = move
        self.save()
        if self.invoice:
            move_line, = [l for l in move.lines
                if l.account == self.invoice.account]
            self._reconcile_line(move_line)
        return move

    def _check_invoice_amount_to_pay(self):
        pool = Pool()
        Currency = pool.get('currency.currency')
        Lang = pool.get('ir.lang')

        if not self.invoice:
            return
        with Transaction().set_context(date=self.invoice.currency_date):
            amount_to_pay = Currency.compute(self.invoice.currency,
                self.invoice.amount_to_pay,
                self.line.company_currency)
        if abs(amount_to_pay) < abs(self.amount):
            lang, = Lang.search([
                    ('code', '=', Transaction().language),
                    ])

            amount = Lang.format(lang,
                '%.' + str(self.line.company_currency.digits) + 'f',
                self.amount, True)
            raise UserError(gettext(
                'account_bank_statement_account.'
                'amount_greater_invoice_amount_to_pay',
                amount=amount))

    def _get_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')

        period_id = Period.find(self.line.company.id, date=self.date)

        move_lines = self._get_move_lines()
        return Move(
            period=period_id,
            journal=self.line.journal.journal,
            date=self.date,
            lines=move_lines,
            description=self.description,
            )

    def _get_move_lines(self):
        '''
        Return the move lines for the statement line
        '''
        pool = Pool()
        MoveLine = pool.get('account.move.line')

        move_line = self._get_move_line()
        journal = self.line.journal
        account = journal.account

        if not account:
            raise UserError(gettext(
                'account_bank_statement.account_statement_journal',
                journal=journal.rec_name))
        if not account.bank_reconcile:
            raise UserError(gettext(
                'account_bank_statement.account_not_bank_reconcile',
                journal=journal.rec_name))
        if self.account == account:
            raise UserError(gettext(
                'account_bank_statement.same_account',
                account=self.account.rec_name,
                line=self.rec_name,
                journal=self.line.journal.rec_name))

        amount_second_currency = None
        if move_line.amount_second_currency:
            amount_second_currency = -move_line.amount_second_currency
        bank_move = MoveLine(
            description=self.description,
            debit=self.amount >= _ZERO and self.amount or _ZERO,
            credit=self.amount < _ZERO and -self.amount or _ZERO,
            account=account,
            party=(self.party or self.line.company.party
                if account.party_required else None),
            second_currency=move_line.second_currency,
            amount_second_currency=amount_second_currency,
            )
        return [bank_move, move_line]

    def _get_move_line(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Currency = Pool().get('currency.currency')

        if self.line.journal.currency != self.line.company.currency:
            second_currency = self.line.journal.currency.id
            with Transaction().set_context(date=self.line.date.date()):
                amount_second_currency = abs(Currency.compute(
                        self.line.company.currency, self.amount,
                        self.line.journal.currency))
                if self.amount >= _ZERO:
                    amount_second_currency = -amount_second_currency
        else:
            amount_second_currency = None
            second_currency = None

        return MoveLine(
            description=self.description,
            debit=self.amount < _ZERO and -self.amount or _ZERO,
            credit=self.amount >= _ZERO and self.amount or _ZERO,
            account=self.account,
            party=self.party if self.account.party_required else None,
            second_currency=second_currency,
            amount_second_currency=amount_second_currency,
        )

    def _reconcile_line(self, line):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Currency = pool.get('currency.currency')
        Invoice = pool.get('account.invoice')

        self._check_invoice_amount_to_pay()
        with Transaction().set_context(date=self.invoice.currency_date):
            amount = Currency.compute(self.line.journal.currency,
                self.amount, self.line.company_currency)

        reconcile_lines = self.invoice.get_reconcile_lines_for_amount(
            amount)
        if reconcile_lines[1] == Decimal('0.0'):
            lines = reconcile_lines[0] + [line]
            MoveLine.reconcile(lines)
        Invoice.write([self.invoice], {
            'payment_lines': [('add', [line.id])],
        })
